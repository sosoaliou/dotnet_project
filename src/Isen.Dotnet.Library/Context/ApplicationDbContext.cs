using System.Diagnostics.CodeAnalysis;
using Isen.Dotnet.Library.Model;
using Microsoft.EntityFrameworkCore;

namespace Isen.Dotnet.Library.Context
{    
    public class ApplicationDbContext : DbContext
    {        
        public DbSet<Person> PersonCollection { get; set; }
        public DbSet<Service> ServiceCollection { get; set; }
        public DbSet<Role> RoleCollection { get; set; }

        public ApplicationDbContext(
            [NotNullAttribute] DbContextOptions options) : 
            base(options) {  }

        protected override void OnModelCreating(
            ModelBuilder modelBuilder){

            // OneToMany : Person/Service
            modelBuilder
                .Entity<Person>()
                .ToTable(nameof(Person))
                .HasKey(p => p.Id);
            modelBuilder
                .Entity<Person>()
                .HasOne(p => p.Service)
                .WithMany()
                .HasForeignKey(p => p.ServiceId);

            // ManyToMany : Person/Role
            modelBuilder
                .Entity<PersonRole>()
                .ToTable(nameof(PersonRole))
                .HasKey(pr => new { pr.PersonId, pr.RoleId });
            modelBuilder
                .Entity<PersonRole>()
                .HasOne(pr => pr.Role)
                .WithMany(r => r.PersonRoles)
                .HasForeignKey(pr => pr.RoleId);
            modelBuilder
                .Entity<PersonRole>()
                .HasOne(pr => pr.Person)
                .WithMany(p => p.PersonRoles)
                .HasForeignKey(pr => pr.PersonId);  
            }
        }

    }
