using System;
using System.Collections.Generic;
using System.Linq;
using Isen.Dotnet.Library.Context;
using Isen.Dotnet.Library.Model;
using Microsoft.Extensions.Logging;

namespace Isen.Dotnet.Library.Services
{
    public class DataInitializer : IDataInitializer
    {
        // Générateur aléatoire
        private readonly Random _random;

        // DI de ApplicationDbContext
        private readonly ApplicationDbContext _context;
        private readonly ILogger<DataInitializer> _logger;
        public DataInitializer(
            ILogger<DataInitializer> logger,
            ApplicationDbContext context)
        {
            _context = context;
            _logger = logger;
            _random = new Random();
        }

        private List<string> _firstNames => new List<string>
        {
            "Sofiane", "Anne", "David", "Pierre", "Vincent", "Hadrien", "Lilian", "Louis", "Alicia"
        };
        private List<string> _lastNames => new List<string>
        {
            "Aliouche", "Arbousset", "Jubert", "Laxe", "Ceccarelli", "Grandvoinet", "Sarrazin", "Vuuil"
        };

        // Random FirstName
        private string RandomFirstName =>
            _firstNames[_random.Next(_firstNames.Count)];

        //Random LastName
        private string RandomLastName =>
            _lastNames[_random.Next(_lastNames.Count)];

        // Random Date
        private DateTime RandomDate =>
            new DateTime(_random.Next(1960, 2000), 1, 1)
                .AddDays(_random.Next(0, 365));

        // Random Mobile Number
        private string RandomMobileNumber => 
            $"06{_random.Next(10000000, 99999999)}";

        // List Services
        public List<Service> GetServices() {
            return  new List<Service> 
            {
                new Service { Name = "Production"},
                new Service { Name = "Developpement"},
                new Service { Name = "Marketing"},
                new Service { Name = "Communication"},
                new Service { Name = "Commerce"},
            };
        }

        // List Roles
        public List<Role> GetRoles() {
            return new List<Role> 
            {
                new Role { Name = "Developpeur"},
                new Role { Name = "SuperAdministrateur"},
                new Role { Name = "Utilisateur" },
                new Role { Name = "Administrateur" },
                new Role { Name = "Manager" },
            };
        }

        // Random Services
        private Service RandomService
        {
            get
            {
                var services = _context.ServiceCollection.ToList();
                return services[_random.Next(services.Count)];
            }
        }

        // Random Roles
        private Role RandomRole
        {
            get {
                var roles = _context.RoleCollection.ToList();
                return roles[_random.Next(roles.Count)];
            }
        }

        // Random Persons
        private Person RandomPerson
        {
            get {
                var service = RandomService;
                var role = RandomRole;
                var firstName = RandomFirstName;
                var lastName = RandomLastName;
                var person = new Person()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = RandomDate,
                    MobileNumber = RandomMobileNumber,
                    Service = service,
                    ServiceId = service.Id,
                    PersonRoles = new List<PersonRole>()
                };
                var personRole = new PersonRole {
                    Person = person,
                    PersonId = person.Id,
                    Role = role,
                    RoleId = role.Id
                };
                person.PersonRoles.Add(personRole);

                return person;
            }
        }
 
        // List Persons
        public List<Person> GetPersons()
        {
            var persons = new List<Person>();
            for (var i = 0; i < _firstNames.Count; i++)
            {
                persons.Add(RandomPerson);
            }
            return persons;
        }

        public void DropDatabase()
        {
            _logger.LogWarning("Dropping database");
            _context.Database.EnsureDeleted();
        }
        public void CreateDatabase()
        {
            _logger.LogWarning("Creating database");
            _context.Database.EnsureCreated();
        }

        public void AddPersons()
        {
            _logger.LogWarning("Adding persons...");
            if (_context.PersonCollection.Any()) return;
            var persons = GetPersons();
            _context.AddRange(persons);
            _context.SaveChanges();
        }

        public void AddRoles()
        {
            _logger.LogWarning("Adding roles");
            if (_context.RoleCollection.Any()) return;
            var roles = GetRoles();
            _context.AddRange(roles);
            _context.SaveChanges();
        }

        public void AddServices()
        {
            _logger.LogWarning("Adding services");
            if (_context.ServiceCollection.Any()) return;
            var services = GetServices();
            _context.AddRange(services);
            _context.SaveChanges();
        }
    }
}