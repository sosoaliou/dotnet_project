using System.Collections.Generic;
using Isen.Dotnet.Library.Model;

namespace Isen.Dotnet.Library.Services
{
    public interface IDataInitializer
    {
         List<Person> GetPersons();
         List<Service> GetServices();
         List<Role> GetRoles();
         void DropDatabase();
         void CreateDatabase();
         void AddPersons();
         void AddRoles();
         void AddServices();
    }
}