using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Text;

namespace Isen.Dotnet.Library.Model
{
    public class Person : BaseEntity
    {        
        public int? PersonId { get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Mail => $"{FirstName}@gmail.com".ToLower();
        public DateTime? DateOfBirth {get;set;}
        public string MobileNumber {get;set;}

        public Service Service {get;set;}
        public int? ServiceId {get;set;}
        public List<PersonRole> PersonRoles { get; set; }
        
        [NotMapped]
        public string ToStringRoles {
            get {
                var builder = new StringBuilder();
                foreach(var personRole in PersonRoles) {
                    builder.Append(personRole.Role.Name);
                    builder.AppendLine();
                }
                return builder.ToString();
            }
        }        
    }
}