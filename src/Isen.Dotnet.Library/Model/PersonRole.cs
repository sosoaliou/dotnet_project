namespace Isen.Dotnet.Library.Model
{
    public class PersonRole
    {
        public int RoleId {get;set;}
        public Role Role { get;set; }
        
        public int PersonId { get;set;}
        public Person Person { get;set;}
       
    }
}