using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Isen.Dotnet.Library.Model
{
    public class Role : BaseEntity
    {
        public int RoleId {get;set;}
        public string Name { get;set; }
        public IList<PersonRole> PersonRoles { get; set; }

        [NotMapped]
        public string ToStringPersons {
            get {
                var builder = new StringBuilder();
                foreach(var personRole in PersonRoles) {
                    builder.Append(personRole.Person.FirstName + " " + personRole.Person.LastName + "; ");
                    builder.AppendLine();
                }
                return builder.ToString();
            }
        }
    }
}